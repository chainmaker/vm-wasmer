module chainmaker.org/chainmaker/vm-wasmer-test/v2

go 1.15

require (
	chainmaker.org/chainmaker/chainconf/v2 v2.3.4
	chainmaker.org/chainmaker/common/v2 v2.3.5
	chainmaker.org/chainmaker/localconf/v2 v2.3.5
	chainmaker.org/chainmaker/logger/v2 v2.3.4
	chainmaker.org/chainmaker/pb-go/v2 v2.3.6
	chainmaker.org/chainmaker/protocol/v2 v2.3.6
	chainmaker.org/chainmaker/store/v2 v2.3.6
	chainmaker.org/chainmaker/utils/v2 v2.3.5
	chainmaker.org/chainmaker/vm-wasmer/v2 v2.3.6
	chainmaker.org/chainmaker/vm/v2 v2.3.6
	github.com/gogo/protobuf v1.3.2
	github.com/mitchellh/mapstructure v1.5.0
	github.com/mr-tron/base58 v1.2.0
	github.com/spf13/cobra v1.1.1
)

replace (
	chainmaker.org/chainmaker/vm-wasmer/v2 => ../
	github.com/RedisBloom/redisbloom-go => chainmaker.org/third_party/redisbloom-go v1.0.0
	google.golang.org/grpc => google.golang.org/grpc v1.26.0
)
